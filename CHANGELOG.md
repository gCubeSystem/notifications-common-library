
# Changelog for Notifications Common Library

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.5.0] - 2022-06-25
 
 - Ported to git

## [v1.4.0] - 2017-02-01

 - UserManager/GroupManager implementations need to be provided as input of several constructors
 
 - Added thread to schedule job notifications
 
 - Post notifications thread now accepts Set instead of List for mentioned vre groups and hashtags to avoid duplicates

## [v1.2.1] - 2016-02-29
 
 - Added shared method for posting notifications
 - Added support to discussion into email's body

## [v1.1.0] - 2015-10-13
 
 - Added shared method for messaging notifications

## [v1.0.0] - 2015-06-30

 - First release
