package org.gcube.portal.notifications.thread;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.gcube.applicationsupportlayer.social.NotificationsManager;
import org.gcube.portal.databook.server.DatabookStore;
import org.gcube.portal.databook.shared.Comment;
import org.gcube.portal.databook.shared.Like;
import org.gcube.vomanagement.usermanagement.UserManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Massimiliano Assante ISTI-CNR
 *
 */
public class CommentNotificationsThread implements Runnable {

	private static final Logger _log = LoggerFactory.getLogger(CommentNotificationsThread.class);

	String commenterUserId;
	String commentedFeedId;
	private String commentText;
	private String postOwnerId;
	private NotificationsManager nm;
	private String commentKey;
	private HashSet<String> userIdsToNotify;
	//needed to avoid sending notification twice (the user who favorited gets the notification anyways)
	private ArrayList<Like> likes;
	private UserManager userManager;

	/**
	 * 
	 * @param storeInstance
	 * @param userManager
	 * @param commenterUserId
	 * @param commentedPostId
	 * @param commentText
	 * @param nm
	 * @param postOwnerId
	 * @param commentKey
	 * @param likes
	 */
	public CommentNotificationsThread(DatabookStore storeInstance, UserManager userManager, String commenterUserId, 
			String commentedPostId, String commentText, NotificationsManager nm, String postOwnerId, String commentKey, ArrayList<Like> likes) {
		super();
		this.nm = nm;
		this.commenterUserId = commenterUserId;
		this.commentedFeedId = commentedPostId;
		this.commentText = commentText;
		this.postOwnerId = postOwnerId;
		this.commentKey = commentKey;
		this.likes = likes;
		this.userManager = userManager;

		userIdsToNotify = new HashSet<String>();
		List<Comment> postComments = storeInstance.getAllCommentByFeed(commentedPostId);
		for (Comment comment : postComments) {
			if (comment.getUserid().compareTo(commenterUserId) != 0) {
				userIdsToNotify.add(comment.getUserid());
			}
		}

		//clean
		//this.comments = comments;
	}

	@Override
	public void run() {
		String postOwnerFullName = "";
		try {
			postOwnerFullName = userManager.getUserByUsername(postOwnerId).getFullname();
		} catch (Exception e) {
			postOwnerFullName = postOwnerId;
		} 
		//get the list of userid who liked the post
		ArrayList<String> favoriteUserIds = new ArrayList<>();
		for (Like favorite : likes) {
			favoriteUserIds.add(favorite.getUserid());
		}
		
		if (userIdsToNotify != null) {
			for (String userId : userIdsToNotify) {	
				if (userId.compareTo(postOwnerId) != 0 && !(favoriteUserIds.contains(userId)) ) { //avoid notifying the owner and the user who liked twice 
					boolean result = nm.notifyCommentReply(userId, commentedFeedId, commentText, postOwnerFullName, postOwnerId, commentKey);
					_log.trace("Sending Notification for also commented to: " + postOwnerFullName + " result?"+ result);
				}
			}
		}

	}	
}
